import statement from '../src/statement';
import plays from './fixtures/plays.json';
import invoices from './fixtures/invoices.json';

describe('Generating a statement', () => {
  const expected = `Statement for BigCo
  Hamlet: $650.00 (55 seats)
  As You Like It: $580.00 (35 seats)
  Othello: $500.00 (40 seats)
Amount owed is $1,730.00
You earned 47 credits
`;

  it('should create the desired output', () => {
    invoices.map((invoice) => {
      const result = statement(invoice, plays);
      // console.log(result);
      expect(result).toEqual(expected);
      return null;
    });
  });
});
